//libraries
log4js = require('log4js'),
_ = require('underscore');
dbSrc = require('./node-mysql-q');

//settings
var startTimer;
var dbConn = {
	host     : 'localhost',
	user     : 'root',
	password : 'root',
	// debug    : true,
	// database : 'dbname',
	printQueryInfo : true, //activate logs
	connectionLimit: 10
};

//init
log4js.configure({
	appenders: [ { type: "console" }],
	replaceConsole: true
});

var logger = log4js.getLogger()
pErr = function(err){ console.error('err', err); };

// App
var dbPool = new dbSrc(dbConn),
startTimer = new Date();

queryTest = function(){
	_.each(_.range(1,21), function(i){
		console.log('queryDb');
		dbPool.queryDb('select sleep(1)')
			.then(
				function(rows, fields){
					console.log('done q'+i, new Date() - startTimer + 'ms');
				}, pErr
			);
	})
	// dbPool.all();
};

//start
queryTest();


