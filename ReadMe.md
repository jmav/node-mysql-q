# node-mysql-q

node-mysql-q wraps mysql driver with promises [Q module](https://github.com/kriskowal/q). It creates pool of connections from which queryes can be called.


## Installation

    npm install git@bitbucket.org:jmav/node-mysql-q.git --save


## Usage

	//libraries
	var dbSrc = require('./node-mysql-q');

	//settings
	var dbConn = {
		host     : 'localhost',
		user     : 'root',
		password : 'root',
		printQueryInfo : true, //activate logs
		connectionLimit: 10
	};

	//APP
	dbPool.queryDb('select sleep(1)')
		.then(
			function(rows, fields){
				console.log('done');
			}, function(err){
				console.log('Error', err);
			}
		);

###Example

Example triggers 20 queries to mysql. Queries are randomly executed, responses are visible in console window. 

**Config**

Set dbConn parameters in example.js to connect to DB. Query uses sleep sentance which doesn't need any data structure in DB.

	var dbConn = {
		host     : 'localhost',
		user     : 'root',
		password : 'root',
		// debug    : true,
		// database : 'dbname',
		printQueryInfo : true, //activate logs
		connectionLimit: 10
	};

**Run example code**

    node example.js

### Contributors

jmav (Jure Mav)